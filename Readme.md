# Wireframing Kit for Adobe XD

![](https://jaison.info/images/article/wireframing_kit/wireframing-kit-v3-demo.png)

Wireframing is an important skill in the software industry, used to organise information on an interface and communicate it with the team. Balsamiq, UXPin, Omnigraffle, Axure etc. are few of the already established wireframing apps. So, why did I put effort creating this wireframe kit?

The main reason is to avoid multiple tool dependency when creating design artefacts like wireframes, mockups, etc. I have used Balsamiq for wireframing, Sketch for creating mockups and Keynote for prototyping/flow simulation. But the challenge is to manage and bridge these softwares.

When Adobe released their new tool called Adobe Experience Design *(a.k.a Adobe XD)* last year, I gave it a try. I used the tool to design screens for my personal project, and it gave me a single seamless platform to develop screens and simulate the flow between them. This feature got me thinking about doing wireframing on Adobe XD, so that I can create wireframes and corresponding mockups within one platform. On top of that, Adobe XD offers static prototyping, which allows you to conduct user testing very early in the development cycle, even at the stage of wireframing.

### Download

* [Version 3]()
* [Version 2]()
* [Version 1]()

### List of Components

* Alert / Confirm
* Alert / Prompt
* Avatar
* Blockquote
* Breadcrumb
* Button / Inline
* Button / Primary
* Button / Secondary
* Button / Split
* Captcha
* Card
* Chart Grid
* Checkbox / Checked
* Checkbox / Unchecked
* Code Snippet
* Color Picker
* Dropdown
* File Uploader / Control
* File Uploader / Dropzone
* File Uploader / File Listing
* Label
* List / Ordered
* List / Unordered
* Loading
* Notification / Toast
* Pagination
* Player / Audio
* Player / Video
* Popover / Calendar
* Popover / Color
* Popover / List
* Progress Bar
* Radio Button / Checked
* Radio Button / Unchecked
* Rating / Heart / Empty
* Rating / Heart / Filled
* Rating / Heart / Partial Filled
* Rating / Star / Empty
* Rating / Star / Filled
* Rating / Star / Partial Filled
* Segment Control / Left
* Segment Control / Middle
* Segment Control / Right
* Slider
* Social Share
* Table / Cell / Body
* Table / Cell / Header
* Tag / Basic
* Tag / Close-able
* Text Area
* Text Input / Basic
* Text Input / Date Picker
* Text Input / Numeric
* Tooltip
* World Map
* WYSWYG